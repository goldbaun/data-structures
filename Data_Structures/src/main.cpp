#include <cstdlib>
#include <iostream>
#include <list>
#include <queue>
#include <stack>

int main(int argc, char* argv[])
{
    int k = 0;
    int m = 0;
    int option1 = 0;
    int option3 = 0;
    std::stack<int> stackSTL;
    std::queue<int> queueSTL;
    std::list<int> listSTL;
    std::cout << "Struktury danych" << std::endl;
    while(option1 != 4)
    {
        std::cout << "Wybierz strukture danych" << std::endl;
        std::cout << "1 - Stos" << std::endl;
        std::cout << "2 - Kolejka" << std::endl;
        std::cout << "3 - Lista" << std::endl;
        std::cout << "4 - Zakoncz" << std::endl;
        std::cin >> option1;
        while(option1 != 4)
        {
            switch(option1)
            {
                case 1: // STL Stack
                    std::cout << "===================================" << std::endl;
                    std::cout << "Wybierz operacje" << std::endl;
                    std::cout << "1 - Dodaj losowy element" << std::endl;
                    std::cout << "2 - Dodaj m-elementow" << std::endl;
                    std::cout << "3 - Wyswietl element na szczycie stosu" << std::endl;
                    std::cout << "4 - Usun element ze szczytu" << std::endl;
                    std::cout << "5 - Usun k-elementow ze szczytu" << std::endl;
                    std::cout << "6 - Wyswietl liczbe elementow na stosie" << std::endl;
                    std::cout << "===================================" << std::endl;
                    std::cin >> option3;
                    switch(option3)
                    {
                        case 1:
                            stackSTL.push(rand());
                            break;

                        case 2:
                            std::cout << "Ile elementow dodac?" << std::endl;
                            std::cin >> m;
                            for(int i = 0; i < m; i++)
                            {
                                stackSTL.push(rand());
                            }
                            break;

                        case 3:
                            if(stackSTL.empty())
                            {
                                std::cout << "Stos jest pusty" << std::endl;
                            }
                            else
                            {
                                std::cout << stackSTL.top() << std::endl;
                            }
                            break;

                        case 4:
                            if(stackSTL.empty())
                            {
                                std::cout << "Stos jest pusty" << std::endl;
                            }
                            else
                            {
                                stackSTL.pop();
                            }
                            break;

                        case 5:
                            std::cout << "Ile elementow usunac?" << std::endl;
                            std::cin >> k;
                            for(int i = 0; (i < k) && !stackSTL.empty(); i++)
                            {
                                stackSTL.pop();
                            }
                            break;

                        case 6:
                            std::cout << "Ilosc elementow na stosie: " << stackSTL.size() << std::endl;
                        default:
                            break;
                    }
                    break;
                case 2: // STL Queue
                    std::cout << "===================================" << std::endl;
                    std::cout << "Wybierz operacje" << std::endl;
                    std::cout << "1 - Dodaj losowy element" << std::endl;
                    std::cout << "2 - Dodaj m-elementow" << std::endl;
                    std::cout << "3 - Usun element z konca kolejki" << std::endl;
                    std::cout << "4 - Usun k-elementow z konca kolejki" << std::endl;
                    std::cout << "5 - Wyswietl element na poczatku kolejki" << std::endl;
                    std::cout << "6 - Wyswietl liczbe elementow w kolejce" << std::endl;
                    std::cout << "===================================" << std::endl;
                    std::cin >> option3;
                    switch(option3)
                    {
                        case 1:
                            queueSTL.push(rand());
                            break;

                        case 2:
                            std::cout << "Ile elementow dodac?" << std::endl;
                            std::cin >> m;
                            for(int i = 0; i < m; i++)
                            {
                                queueSTL.push(rand());
                            }
                            break;

                        case 3:
                            if(queueSTL.empty())
                            {
                                std::cout << "Kolejka jest pusta" << std::endl;
                            }
                            else
                            {
                                queueSTL.pop();
                            }
                            break;

                        case 4:
                            std::cout << "Ile elementow usunac?" << std::endl;
                            std::cin >> k;
                            for(int i = 0; (i < k) && !queueSTL.empty(); i++)
                            {
                                queueSTL.pop();
                            }
                            break;

                        case 5:
                            if(queueSTL.empty())
                            {
                                std::cout << "Kolejka jest pusta" << std::endl;
                            }
                            else
                            {
                                std::cout << queueSTL.front() << std::endl;
                            }
                            break;

                        case 6:
                            std::cout << "Ilosc elementow w kolejce: " << queueSTL.size() << std::endl;
                        default:
                            break;
                    }
                    break;

                case 3: // STL List
                    std::cout << "===========================================" << std::endl;
                    std::cout << "Wybierz operacje" << std::endl;
                    std::cout << "1 - Dodaj losowy element na poczatku listy" << std::endl;
                    std::cout << "2 - Dodaj losowy element na koncu listy" << std::endl;
                    std::cout << "3 - Dodaj m-elementow na poczatku listy" << std::endl;
                    std::cout << "4 - Dodaj m-elementow na koncu listy" << std::endl;
                    std::cout << "5 - Usun element z konca listy" << std::endl;
                    std::cout << "6 - Usun k-elementow z konca listy" << std::endl;
                    std::cout << "7 - Usun wszystkie elementy" << std::endl;
                    std::cout << "8 - Wyswietl zawartosc listy" << std::endl;
                    std::cout << "9 - Wyswietl liczbe elementow listy" << std::endl;
                    std::cout << "10 - Wyswietl ostatni element listy" << std::endl;
                    std::cout << "11 - Wyswietl pierwszy element listy" << std::endl;
                    std::cout << "===========================================" << std::endl;
                    std::cin >> option3;
                    switch(option3)
                    {
                        case 1:
                            listSTL.push_front(rand());
                            break;

                        case 2:
                            listSTL.push_back(rand());
                            break;

                        case 3:
                            std::cout << "Ile elementow dodac?" << std::endl;
                            std::cin >> m;
                            for(int i = 0; i < m; i++)
                            {
                                listSTL.push_front(rand());
                            }
                            break;

                        case 4:
                            std::cout << "Ile elementow dodac?" << std::endl;
                            std::cin >> m;
                            for(int i = 0; i < m; i++)
                            {
                                listSTL.push_back(rand());
                            }
                            break;

                        case 5:
                            if(listSTL.empty())
                            {
                                std::cout << "Lista jest pusta" << std::endl;
                            }
                            else
                            {
                                listSTL.pop_back();
                            }
                            break;

                        case 6:
                            std::cout << "Ile elementow usunac?" << std::endl;
                            std::cin >> k;
                            for(int i = 0; (i < k) && !listSTL.empty(); i++)
                            {
                                listSTL.pop_back();
                            }
                            break;
                        case 7:
                            listSTL.clear();
                            std::cout << "Usunieto wszystkie elementy" << std::endl;
                            break;

                        case 8:
                            if(listSTL.empty())
                            {
                                std::cout << "Lista jest pusta" << std::endl;
                            }
                            else
                            {
                                std::cout << "Zawartosc listy: " << std::endl;
                                for(int i : listSTL)
                                {
                                    std::cout << i << " ";
                                }
                                std::cout << std::endl;
                            }
                            break;

                        case 9:
                            std::cout << "Ilosc elementow na liscie: " << listSTL.size() << std::endl;
                            break;

                        case 10:
                            if(listSTL.empty())
                            {
                                std::cout << "Lista jest pusta" << std::endl;
                            }
                            std::cout << listSTL.back() << std::endl;
                            ;
                            break;

                        case 11:
                            if(listSTL.empty())
                            {
                                std::cout << "Lista jest pusta" << std::endl;
                            }
                            std::cout << listSTL.front() << std::endl;
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        }
    }

    return 0;
}
