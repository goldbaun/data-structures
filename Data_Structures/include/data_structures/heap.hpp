#ifndef DATA_STRUCTURES_HEAP_HPP
#define DATA_STRUCTURES_HEAP_HPP

#define MAX_HEAP_SIZE 10

template <typename T>
class Heap
{
  private:
    T* heapArray = new T[MAX_HEAP_SIZE];
    int heapSize = 0;
    int& refHeapSize = heapSize;

  public:
    void insert(const T& newElement);
    void removeMax();
    std::size_t size();
    bool empty();
    T& operator[](int index);
};

template <typename T>
void Heap<T>::insert(const T& newElement)
{
    int index = heapSize + 1;
    T parent = heapArray[index / 2];
    if(heapSize == 0)
    {
        heapArray[1] = newElement;
        refHeapSize += 1;
    }
    else
    {
        if(newElement < parent)
        {
            heapArray[heapSize] = newElement;
        }
        else
        {
            while(!(newElement < parent))
            {
                heapArray[index] = parent;
                index /= 2;
                heapArray[index] = newElement;
                parent = heapArray[index / 2];
            }
        }
        refHeapSize += 1;
    }
}

template <typename T>
void Heap<T>::removeMax()
{
    int i = 1;
    T tmp;
    heapArray[i] = heapArray[heapSize];
    if(heapArray[2 * i + 1] > heapArray[i])
    {
        tmp = heapArray[2 * i + 1];
        heapArray[2 * i + 1] = heapArray[i];
        heapArray[i] = tmp;
    }
    else
    {
        tmp = heapArray[2 * i];
        heapArray[2 * i] = heapArray[i];
        heapArray[i] = tmp;
    }

    refHeapSize -= 1;
}

template <typename T>
std::size_t Heap<T>::size()
{
    return heapSize;
}

template <typename T>
bool Heap<T>::empty()
{
    return heapSize == 0 ? true : false;
}

template <typename T>
T& Heap<T>::operator[](int index)
{
    return heapArray[index];
}

#endif // DATA_STRUCTURES_HEAP_HPP
