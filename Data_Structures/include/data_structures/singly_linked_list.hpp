#ifndef DATA_STRUCTURES_SINGLY_LINKED_LIST_HPP
#define DATA_STRUCTURES_SINGLY_LINKED_LIST_HPP

#include "list_element.hpp"

template <typename T>
class SinglyLinkedList
{
  private:
    ListElement<T>* head = nullptr; // head wskazuje na pierwszy element listy i ma indeks 0
    int elementCounter = 0;
    int& refElementCounter = elementCounter;

  public:
    bool empty();
    std::size_t size();
    void addFront(const T& newElement);
    void removeFront();
    void addBack(const T& newElement);
    void removeBack();
    const T& front() const;
    const T& back() const;
    void insert(const T& newElement, int index);
    void remove(const T& element);
    T& operator[](int index);
};

template <typename T>
bool SinglyLinkedList<T>::empty()
{
    return elementCounter == 0 ? true : false;
}

template <typename T>
std::size_t SinglyLinkedList<T>::size()
{
    return elementCounter;
}

template <typename T>
void SinglyLinkedList<T>::addFront(const T& newElement)
{
    if(elementCounter == 0)
    {
        head = new ListElement<T>(newElement);
        refElementCounter += 1;
    }
    else
    {
        ListElement<T>* tmp;
        tmp = head;
        for(int i = 0; i < elementCounter - 1; i++)
        {
            tmp = tmp->nextElement;
        }
        tmp->nextElement = new ListElement<T>(newElement);
        refElementCounter += 1;
    }
}

template <typename T>
void SinglyLinkedList<T>::removeFront()
{
    ListElement<T>* tmp;
    tmp = head;
    for(int i = 0; i < elementCounter - 1; i++)
    {
        tmp = tmp->nextElement;
    }
    delete tmp->nextElement;
    tmp->nextElement = nullptr;
    refElementCounter -= 1;
}

template <typename T>
void SinglyLinkedList<T>::addBack(const T& newElement) // dodaje element przed head
{
    if(elementCounter == 0)
    {
        head = new ListElement<T>(newElement);
        refElementCounter += 1;
    }
    else
    {
        ListElement<T>* newHead;
        newHead = new ListElement<T>(newElement);
        newHead->nextElement = head;
        head = newHead;
        refElementCounter += 1;
    }
}

template <typename T>
void SinglyLinkedList<T>::removeBack() // usuwa head i ustawia head jako nastepny  element
{
    ListElement<T>* newHead;
    newHead = head->nextElement;
    delete head;
    head = newHead;
    refElementCounter -= 1;
}

template <typename T>
const T& SinglyLinkedList<T>::front() const
{
    ListElement<T>* tmp;
    tmp = head;
    for(int i = 0; i < elementCounter - 1; i++)
    {
        tmp = tmp->nextElement;
    }
    return tmp->key;
}

template <typename T>
const T& SinglyLinkedList<T>::back() const // zwraca klucz znajdujacy sie w head
{
    return head->key;
}

template <typename T>
void SinglyLinkedList<T>::insert(const T& newElement, int index)
{
    ListElement<T>* tmp;
    ListElement<T>* newElem;
    tmp = head;
    for(int i = 0; i < index; i++)
    {
        tmp = tmp->nextElement;
    }
    newElem = new ListElement<T>(newElement);
    newElem->nextElement = tmp->nextElement;
    tmp->nextElement = newElem;
}

template <typename T>
void SinglyLinkedList<T>::remove(const T& element)
{
    ListElement<T>* tmp1;
    ListElement<T>* tmp2;
    tmp1 = head;
    tmp2 = head;
    if(head->key == element)
    {
        head = head->nextElement;
        refElementCounter -= 1;
    }
    for(int i = 0; i < (elementCounter - 1); i++)
    {
        if(tmp1->key == element)
        {
            for(int j = 0; j < i; j++)
            {
                tmp2 = tmp2->nextElement;
                tmp2->nextElement = tmp1->nextElement;
            }
            tmp2 = head;
            refElementCounter -= 1;
        }
        tmp1 = tmp1->nextElement;
    }
}

template <typename T>
T& SinglyLinkedList<T>::operator[](int index)
{
    ListElement<T>* tmp;
    tmp = head;
    if(index == 0)
    {
        return tmp->key;
    }
    else
    {
        for(int i = 0; i < index; i++)
        {
            tmp = tmp->nextElement;
        }
        return tmp->key;
    }
}
#endif // DATA_STRUCTURES_SINGLY_LINKED_LIST_HPP
