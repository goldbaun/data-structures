#ifndef STACK_HPP_
#define STACK_HPP_


#define DEFAULT_STACK_SIZE 10

template <typename T>
class Stack
{
  private:
    T* stackArray;
    int stackTop = 0;
    int& refStackTop = stackTop;
    int stackSize;
    int& refStackSize = stackSize;

  public:
    Stack();
    explicit Stack(std::size_t initialCapacity);
    void push(const T& newElement);
    T pop();
    bool empty();
    bool full(std::size_t stackCapacity);
    std::size_t size();
    T top();
};

template <typename T>
Stack<T>::Stack()
{
    stackArray = new T[DEFAULT_STACK_SIZE];
    refStackSize = DEFAULT_STACK_SIZE;
}

template <typename T>
Stack<T>::Stack(std::size_t initialCapacity)
{
    stackArray = new T[initialCapacity];
    refStackSize = initialCapacity;
}

template <typename T>
void Stack<T>::push(const T& newElement)
{
    if((stackTop + 1) == stackSize)
    {
        T* newStackArray;
        newStackArray = new T[stackSize * 2];
        for(int i = 0; i < stackSize; i++)
        {
            newStackArray[i] = stackArray[i];
        }
        delete stackArray;
        stackArray = newStackArray;
    }
    refStackTop += 1;
    stackArray[stackTop] = newElement;
}

template <typename T>
T Stack<T>::pop()
{
    T tmp;
    tmp = stackArray[stackTop];
    refStackTop -= 1;
    return tmp;
}

template <typename T>
bool Stack<T>::empty()
{
    return stackTop == 0 ? true : false;
}

template <typename T>
std::size_t Stack<T>::size()
{
    if(this->empty())
    {
        return 0;
    }
    else
    {
        return (stackTop);
    }
}

template <typename T>
T Stack<T>::top()
{
    T tmp = stackArray[stackTop];
    return tmp;
}

template <typename T>
bool Stack<T>::full(std::size_t stackCapacity)
{
    return (stackTop + 1) == stackCapacity ? true : false;
}

#endif /* STACK_HPP_ */
