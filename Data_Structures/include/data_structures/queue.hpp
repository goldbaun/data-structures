#ifndef QUEUE_HPP_
#define QUEUE_HPP_

#define DEFAULT_STACK_SIZE 10

template <typename T>
class Queue
{
  private:
    T* queueArray;
    int queueSize;
    int& refQueueSize = queueSize;
    int queueFront;
    int& refQueueFront = queueFront;
    int queueTail;
    int& refQueueTail = queueTail;

  public:
    Queue();
    explicit Queue(std::size_t initialCapacity);
    void enqueue(const T& newElement);
    T dequeue();
    bool empty();
    std::size_t size();
    T front();
};

template <typename T>
Queue<T>::Queue()
{
    queueArray = new T[DEFAULT_STACK_SIZE];
    refQueueSize = DEFAULT_STACK_SIZE;
    refQueueFront = 0;
    refQueueTail = -1;
}

template <typename T>
Queue<T>::Queue(std::size_t initialCapacity)
{
    queueArray = new T[initialCapacity];
    refQueueSize = initialCapacity;
    refQueueFront = 0;
    refQueueTail = -1;
}

template <typename T>
void Queue<T>::enqueue(const T& newElement)
{
    if(queueFront == queueSize)
    {
        T* newQueueArray;
        newQueueArray = new T[2 * queueSize];
        for(int i = 0; i < queueFront; i++)
        {
            newQueueArray[i] = queueArray[i];
        }
        delete queueArray;
        queueArray = newQueueArray;
        queueArray[queueFront] = newElement;
        refQueueFront += 1;
    }
    else
    {
        queueArray[queueFront] = newElement;
        refQueueFront += 1;
    }
}

template <typename T>
T Queue<T>::dequeue()
{
    T tmp;
    tmp = queueArray[queueTail + 1];
    queueTail += 1;
    return tmp;
}

template <typename T>
bool Queue<T>::empty()
{
    return (queueFront - 1) == queueTail ? true : false;
}

template <typename T>
std::size_t Queue<T>::size()
{
    return (queueFront - (queueTail + 1));
}

template <typename T>
T Queue<T>::front()
{
    T tmp = queueArray[queueFront - 1];
    return tmp;
}

#endif /* QUEUE_HPP_ */
