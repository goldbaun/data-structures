#ifndef DATA_STRUCTURES_LIST_ELEMENT_HPP
#define DATA_STRUCTURES_LIST_ELEMENT_HPP

template<typename T>
class ListElement
{
public:
    ListElement<T>* nextElement;
    ListElement<T>* previousElement;
    T key;
    ListElement(const T& newElement);
};

template<typename T>
ListElement<T>::ListElement(const T& newElement)
{
    this->key = newElement;
    this->nextElement = nullptr;
    this->previousElement = nullptr;
}


#endif // DATA_STRUCTURES_LIST_ELEMENT_HPP